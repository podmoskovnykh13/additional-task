import pandas as pd
from sklearn.datasets import load_iris

iris = load_iris()
data = iris.data
target = iris.target

df = pd.DataFrame(data, columns=iris.feature_names)
df['target'] = target